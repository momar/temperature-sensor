upload_url = "http://192.168.178.10/esp-sensors/push.php"

--- Returns HEX representation of num
function num2hex(num)
    local hexstr = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = num % 16
        s = string.sub(hexstr, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    if (s:len() < 2) then s = '0' .. s end
    return s
end

blink_step = 0
blink_cb = nil
function blink(error, callback) -- LED on ESP chip
    if callback then
        if not error then
            gpio.write(4, gpio.HIGH)
            callback()
            return
        else
            blink_cb = callback
            blink_step = 0
        end
    end

    if blink_step == 0 or blink_step == 2 or blink_step == 4 then
        gpio.write(4, gpio.HIGH)
    elseif blink_step == 1 or blink_step == 3 then
        gpio.write(4, gpio.LOW)
    end

    blink_step = blink_step + 1
    if blink_step == 5 then
        blink_cb()
    else
        tmr.create():alarm(200, tmr.ALARM_SINGLE, blink)
    end
end

global_timeout = 600
function continue(timeout, none)
    if timeout < global_timeout then global_timeout = timeout end
    done = done + 1
    if done >= todo then
        if global_timeout == 20 then
            if none ~= true then
                print("Some completed, trying again in " .. global_timeout .. " seconds")
            end
        else
            print("All completed, trying again in " .. (global_timeout / 60) .. " minutes")
        end
        blink(global_timeout == 20, function() -- LED on ESP chip
            tmr.create():alarm(global_timeout * 1000, tmr.ALARM_SINGLE, update)
        end)
        return true
    end
    return false
end

-- Queue: http://stackoverflow.com/a/18844036
Queue = {}
function Queue.new()
  return {first = 1, last = 0}
end
function Queue.enqueue(q, value)
  local last = q.last + 1
  q.last = last
  q[last] = value
end
function Queue.dequeue(q)
  local first = q.first
  if first > q.last then return nil end
  local value = q[first]
  q[first] = nil -- to allow garbage collection
  q.first = first + 1
  return value
end

http_q = Queue.new()
http_w = false
http_t = 10*60
function http_queue(addr_string, temp)
    Queue.enqueue(http_q, { addr_string = addr_string, temp = temp })
    if not http_w then http_next() end
end
function http_next()
    http_w = true
    local deq = Queue.dequeue(http_q)
    if deq ~= nil then
        print("Uploading value for " .. deq.addr_string)
        http.get(upload_url .. "?sensor=" .. deq.addr_string .. "&value=" .. deq.temp, nil, function(code, data)
            if code == nil or data == nil then
                print "HTTP error."
            else
                print("HTTP " .. code .. ": " .. data)
            end

            if not continue(http_t) then http_next() end
            http_w = false
        end)
        return true
    else
        print("empty queue")

        if not continue(http_t) then http_next() end
        http_w = false
        return true
    end
end
