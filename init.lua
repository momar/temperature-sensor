print("ESP8266 booted. Reason: " .. node.bootreason())
-- Check for GPIO14 override
gpio.mode(5, gpio.INPUT, gpio.PULLUP)
if gpio.read(5) == 0 then
  print("D5 is low (connected to GND), not doing anything.")
else
  print("D5 is high (not connected to GND), continuing as normal.")

  -- Connect to WiFi or stop execution for WiFi setup
  wifi.setmode(wifi.STATION)
  local wifi_config = wifi.sta.getdefaultconfig(true)
  -- Load wifi config from wifi.lua
  if (wifi_config.ssid == "" and (wifi_config.bssid == nil or wifi_config.bssid == "ff:ff:ff:ff:ff:ff") and file.exists("wifi.lc")) then
    print("Using Wifi configuration from wifi.lc.")
    dofile("wifi.lc")
  elseif (wifi_config.ssid == "" and (wifi_config.bssid == nil or wifi_config.bssid == "ff:ff:ff:ff:ff:ff") and file.exists("wifi.lua")) then
    print("Using Wifi configuration from wifi.lua.")
    dofile("wifi.lua")
  elseif (wifi_config.ssid == "" and (wifi_config.bssid == nil or wifi_config.bssid == "ff:ff:ff:ff:ff:ff")) then
    print("No stored Wifi configuration and no wifi.lua or wifi.lc. Please configure Wifi for this node.")
  else
    print("Using stored Wifi configuration.")
  end
  wifi_config = wifi.sta.getdefaultconfig(true)
  if (wifi_config.ssid == "" and (wifi_config.bssid == nil or wifi_config.bssid == "ff:ff:ff:ff:ff:ff")) then
    print("Wifi is not configured. Please manually set a default configuration.")
  else
    if (wifi_config.auto ~= true and wifi.sta.status() ~= 5) then
      wifi.sta.connect() -- Connect automatically even if "auto" is not set.
    end

    -- Wait for connection...
    if (wifi_config.ssid == "") then
      print("Waiting for connection to \"" .. wifi_config.bssid .. "\"...")
    else
      print("Waiting for connection to " .. wifi_config.ssid .. "...")
    end
    local connection_timer = tmr.create()
    connection_timer:alarm(50, tmr.ALARM_AUTO, function(timer)
      if wifi.sta.status() ~= wifi.STA_CONNECTING then

        -- Not connecting anymore. Find out if it was success or error.

        local wifi_types = {"STA_CONNECTING", "STA_WRONGPWD", "STA_APNOTFOUND",
                            "STA_FAIL", "STA_GOTIP"}
        wifi_types[0] = "STA_IDLE"
        if (wifi_types[wifi.sta.status()] == nil) then
          wifi_types[wifi.sta.status()] = "UNKNOWN (" .. wifi.sta.status() .. ")"
        end

        -- Try to get an IP, show the connection status.
        ip = wifi.sta.getip()
        if (ip ~= nil) then
          connection_timer:unregister()
          print("Connection status: " .. wifi_types[wifi.sta.status()] ..
                " (IP: " .. ip .. ")")

          -- Run start file after successful network connection
          if (file.exists("start.lc")) then
            dofile("start.lc")
          elseif (file.exists("start.lua")) then
            dofile("start.lua")
          end
        else -- No IP, no success... :(
          connection_timer:unregister()
          print("Connection status: " .. wifi_types[wifi.sta.status()])
          print("Connection failed. Rebooting in 30 seconds.")
          tmr.create():alarm(30000, tmr.ALARM_SINGLE, function(timer)
            node.restart()
          end)
        end
      end
    end)
  end
end
