if file.exists("functions.lc") then
    dofile("functions.lc")
else
    dofile("functions.lua")
end

update = function()
    gpio.mode(4, gpio.OUTPUT)
    gpio.write(4, gpio.LOW) -- LED on ESP chip

    global_timeout = 600 -- Reset timeout to 10 minutes

    local sensor = require("ds18b20")
    sensor.setup(2)

    local addrs = sensor.addrs()
    done = 0
    todo = table.getn(addrs)
    if (todo < 1) then
        print "No sensor attached. Trying again in 20 seconds..."
        continue(20, true)
        return
    end

    http_t = 10*60
    completed = false

    for i,addr in pairs(addrs) do
        local addr_string = ""
        for b=1,8 do
            addr_string = addr_string .. num2hex(addr:byte(b))
        end
        print("Reading sensor: " .. addr_string)

        local temp = sensor.read(addr, sensor.C)
        if temp == nil then
            print("Invalid temperature for sensor " .. addr_string .. " (nil)")
            http_t = 20
        else
            print(addr_string .. ": " .. temp .. " C")

            if temp == 85 then
                print("Invalid temperature for sensor " .. addr_string .. " (85)")
                http_t = 20
            else
                completed = true
                gpio.write(4, gpio.HIGH)
                http_queue(addr_string, temp)
            end
        end
    end
    if completed == false then
        todo = 0
        print("None completed, trying again in 20 seconds")
        continue(20, true)
    end
end
update()

-- Fischfütterung
gpio.mode(6, gpio.OUTPUT)
gpio.write(6, gpio.HIGH)
gpio.mode(0, gpio.OUTPUT) -- LED on NodeMCU board
tmr.create():alarm(100, tmr.ALARM_SINGLE, function()
    gpio.write(0, gpio.HIGH)
end)

feeding = false

local server = net.createServer(net.TCP, 30)
server:listen(6554, function(conn)
    print("[TCP] Connection established")
    conn:on("receive", function(sck, data)
        if feeding then
            print("[TCP] Already feeding")
            sck:send("already feeding\n")
            return
        end
        gpio.write(0, gpio.LOW)
        if data == "feed:imv7ffjenCDB\n" or data == "feed\n" then
            feeding = true
            print("[TCP] Switching relais on")
            gpio.write(6, gpio.LOW)
            tmr.create():alarm(10000, tmr.ALARM_SINGLE, function (timer)
                print("[TCP] Switching relais off")
                gpio.write(6, gpio.HIGH)
                gpio.write(0, gpio.HIGH)
                feeding = false
            end)
            sck:send("ok\n")
        else
            print("[TCP] Unknown command")
            sck:send("unknown command\n")
            tmr.create():alarm(100, tmr.ALARM_SINGLE, function()
                gpio.write(0, gpio.HIGH)
            end)
        end
    end)
    conn:on("sent", function(sck) sck:close() end)
end)
