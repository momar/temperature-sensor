wifi.sta.getap(1, function(t)
local authmode = { "UNKNOWN ", "WPA     ", "WPA2    ", "WPA/WPA2", "UNKNOWN " }
authmode[0] = "OPEN    "
print("Encryption   BSSID              Channel  RSSI    SSID")
for ssid,info in pairs(t) do
if (info[4] < 10) then info[4] = " " .. info[4] end
print(authmode[info[1]] .. "    " .. info[3] .. "  " .. info[4] .. "       " .. info[2] .. "dBm  " .. ssid)
end
end)
