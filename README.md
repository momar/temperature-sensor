# 1-Wire-Sensor-Node for ESP8266/NodeMCU

Die Sensordaten aller angeschlossener Sensoren werden automatisch alle 10 Minuten an `http://192.168.178.10/esp-sensors/push.php?sensor=<addr>&value=<temp>` gesendet (in `functions.lua` als `upload_url` definiert).

Zusätzlich kann ein Relais per TCP geschaltet werden (Einsatzzweck ist ein Fischfutterautomat, es bleibt dann 10 Sekunden eingeschaltet und geht dann wieder aus), dafür muss `feed` mit einem abschließenden Zeilenumbruch `\n` an Port `6554` der Node gesendet werden:
```
echo feed | nc 192.168.178.** 6554
```

Eine Relay-Node sollte über die Router-Weboberfläche einer festen IP zugewiesen werden.

## Pinout & Teile

![Pinout](pinout.png?)

- NodeMCU ESP8266
- 5V Micro-USB-Netzteil
- DS18B20 Temperatursensor
- 470 kΩ Widerstand als Pull-Up
- Optional: Relais-Board mit 1x Relay

## Build & Flash

Die Anwendung ist in [Lua](https://stigmax.gitbook.io/lua-guide/) entwickelt, und kann somit mit der [NodeMCU-Firmware](https://nodemcu.readthedocs.io/) genutzt werden. Benötigt werden hierbei die Module `file`, `gpio`, `http`, `net`, `node`, `ow` (1-Wire), `tmr` (timer), `uart` und `wifi`, mit dem [NodeMCU Custom Builds](https://nodemcu-build.com/)-Tool kann eine solche Firmware erzeugt werden.

Als Flash-Werkzeug kommen das [esptool](https://github.com/espressif/esptool) sowie das [nodemcu-tool](https://www.npmjs.com/package/nodemcu-tool) zum Einsatz, wofür entsprechend [Python 3](https://www.python.org/downloads/) und [Node.js](https://nodejs.org/de) (LTS-Version ist empfohlen) benötigt werden. Die beiden Tools können dann im Terminal (bzw. der Powershell unter Windows) mit `pip install esptool` sowie `npm install -g nodemcu-tool` installiert werden.

Nach diesen Vorbereitungen muss der serielle Port (z.B. `COM5` unter Windows oder `/dev/ttyUSB0` unter Linux) beispielsweise mit dem Geräte-Manager herausgefunden werden, dann kann die Firmware sowie das Programm wie folgt auf den NodeMCU geladen werden:

1. Firmware installieren (nur bei neuem NodeMCU notwendig):  
   `esptool --port COM** write_flash -fm dio 0x00000 firmware.bin`
2. Dateisystem formatieren:  
   `nodemcu-tool -p COM** mkfs`
3. Dateien (aus diesem Git-Repo) auf den NodeMCU kopieren:  
   `nodemcu-tool -p COM** upload *.lua`
4. Zum Debuggen das NodeMCU-Terminal öffnen:  
   `nodemcu-tool -p COM** terminal`
5. Den Reset-Jumper überbrücken um neuzustarten. Wenn es einen Fehler in `start.lua` gibt, während dem Reset Konsolen-Jumper überbrücken, um direkt in die Konsole zu starten. 

## Sensorliste (für Stefan)

| Farben     | ID                     | Kommentar                                                   |
|------------|------------------------|-------------------------------------------------------------|
| blau+blau  | 0x2815870906000076     | Neuer Sensor                                                |
| blau+rot   | 0x2874edcd04000056     | ~~See-Node: Außentemperatur~~ (???)                         |
| ???        | 0x28bddb81e37d3c2a     | See-Node: Außentemperatur NEU (???)                         |
| blau+gelb  | ~~0x280124f94f2001dd~~ | ~~See-Node: Pool-Temperatur~~ (Entsorgt, Error 85)          |
| ???        | 0x28b4f45704e13c10     | See-Node: Pool-Temperatur NEU                               |
| gelb+gelb  | ~~0x28c24096f0013cba~~ | ~~See-Node: Wasser-Temperatur~~ (Entsorgt, verbindet nicht) |
| ???        | 0x28cc4df0b02207e7     | See-Node: Wasser-Temperatur NEU                             |
| ???        | 0x280b130a0600001e     | Aquarium (Gelb)                                             |
| ???        | 0x28b1950a06000081     | Aquarium (Schwarz)                                          |
| ???        | 0x2863e109060000d1     | Garage (Rot)                                                |
| ???        | 0x287955ce040000e5     | Garage (Schwarz)                                            |

### Sensor-Mapping auf Raspberry Pi (für Stefan)

```
ssh **
cd /var/www/esp-sensors/values
ls -l              # Sensor-IDs & Mappings anzeigen
ln -s <ID> <NAME>  # Mapping erstellen
rm <NAME>          # Mapping entfernen
rm <ID>            # Sensor entfernen (bis zum nächsten Update)
```
